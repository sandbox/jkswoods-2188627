<?php
/**
 * @file
 * Template.php override theme functions.
 */

/**
 * Override of theme_button().
 */
function tuktuk_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = $element['#type'];
  element_set_attributes($element, array('id', 'name', 'value'));
  $element['#attributes']['class'] = ' button';
  return '<input' . drupal_attributes($element['#attributes']) . '/>';
}


/**
 * Override of theme_image().
 */
function tuktuk_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);
  foreach (array('width', 'height', 'alt', 'title') as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }
  $attributes['class'][] = 'responsive';
  return '<img' . drupal_attributes($attributes) . ' />';
}

/**
 * Override of theme_status_message()
 */
function tuktuk_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status Message'),
    'error' => t('Error Message'),
    'warning' => t('Warning Message'),
    );

  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"messages $type alert\">\n";
    if (!empty($status_heading[$type])) {
      $output .= "<h2 class=\"element-invisible\">" . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

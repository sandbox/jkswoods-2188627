<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a
 * single Drupal Page
 *	
 * Variables:
 * - $language: (object) The language the site is being displayed in.
 * - $language->language contains its textual representation
 * - $language->dir contains the language direction. It will either
 *   be 'ltr' or 'rtl'
 * - $rdf_namespaced: All the RDF namespace prefixes used in the HTML
 *   document
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF
 * - $head_title: A modified version of the page title, for use in the
 *   TITLE
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and
 *   settings for the page
 * - $page_top: Initial markup for any modules that have altered the page.
 *   This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup for any modules that have altered
 *   the page. This variable should always be output last, after all other
 *   dynamic content.
 * - $classes: String of classes that can be used to style contextually
 *   through css.
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
  xml:lang="<?php print $language->language; ?>" 
  version="XHTML+RDFa 1.0" 
  dir="<?php print $language->dir; ?>"
  <?php print $rdf_namespaces; ?>>
  <head profile="<?php print $grddl_profile; ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php print $head_title?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes; ?>>
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
  </body>
</html>

<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not found in this
 * template. Instead they can be found in the html.tpl.php
 * template in this directory.
 *	
 * Available variables:
 *
 * Generaral utility variables:
 * - $base_path: The base URL path of the Drupal installation.
 *   At the very least, this will always default to /.
 * - $directory: The directory the template is located in, e.g.
 *   modules/system or themes/bartik
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. USe this instead of
 *   $base_path, when linking to the font page. This includes the
 *   language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme
 *   configuration.
 * - $site_name: The name of the site, empty when display has been
 *   disabled in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has
 *   disabled in them settings.
 *
 * Navigation:
 * - $main_menu (array): An array containeing the main links for the
 *   site, if they have been configured
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page Content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output
 *   populated by modules, intended to be displayed after the main
 *   title tag that appears the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page 
 *   (e.g., the view and edit tabs when displaying a node ).
 * - $action_links (array): Actions local to the page, such as 'Add menu'
 *   on the menu administration interface.
 * - $feed_icons: A string of all feed icons for the current
 *   page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/1245 and node/1245/revisions, but
 *   not comment/reply/12345).
 *	
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see html.tpl.php
 * @see ../tuktuk_core/tuktuk.css
 */
?>
<header>
  <div class="row">
    <div class="column_1 nomargin">
      <?php if (isset($logo)): ?>
      <a href="/"><img src="<?php print $logo ?>" class="logo"/></a>
      <?php endif; ?>
    </div>
    <div class="column_11 text align right nomargin">
      <nav data-tuktuk="menu" class="text bold">
      <?php
      if($main_menu):
        print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'menu',
            'class' => array(
              'links',
              'inline',
              'clearfix')),
          'heading' => NULL));
      endif;
      ?>
      </nav>
    </div>
  </div>
</header>
<div class="padding bck light"></div>
<div class="padding bck color"></div>
<br/>
<div class="row padding align right">
  <?php if (isset($page['highlighted'])): ?>
    <?php print render($page['highlighted']); ?>
  <?php endif; ?>
  <?php if (isset($page['help'])): ?>
    <?php print render($page['help']); ?>
  <?php endif; ?>
  <?php if (isset($breadcrumb)): ?>
    <?php print $breadcrumb; ?>
  <?php endif; ?>
</div>
<div class="row">
  <?php print $messages; ?>
  <?php if ($page['sidebar_first']): ?>
  <aside class="bck light column_3 sidebar">
    <?php print render($page['sidebar_first']); ?>
  </aside>
  <?php endif; ?>
  <?php if ($page['content']): ?>
  <div class="bck lightest column_9">
    <?php if (isset($tabs)): ?>
      <?php print render($tabs); ?>
    <?php endif; ?>
    <?php print render($page['content']); ?>
  </div>
  <?php endif; ?>
</div>
<div class="padding">
  <footer class="bck lightest padding">
    <?php if($page['footer']): ?>
    <?php print render($page['footer']); ?>
    <?php endif; ?>
  </footer>
</div>

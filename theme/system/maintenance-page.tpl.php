<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
    xml:lang="<?php print $language->language; ?>" 
    lang="<?php print $language->language; ?>" 
    dir="<?php print $language->dir; ?>">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $classes; ?> light">
    <div class="text align center">
      <img src="<?php print $logo; ?>" class="logo"/>
      <h1>Site Under Maintenance</h1>
      <h3>Please Check Back Soon!</h3>
    </div>
    <hr/>
  </body>
</html>

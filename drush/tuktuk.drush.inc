<?php

function tuktuk_drush_command() {
  $items = array();

  // Define the command and its arguments/options
  $items['tuktuk'] = array(
    'description' => 'Set up the tuktuk subtheme directory with a drush command.',
    'arguments' => array(
        'name' => 'The name of the subtheme based off of tuktuk.',
       ),
    'options' => array(
        'less' => 'Include the LESS files for building upon',
        'stylus' => 'Include the stylus files for building upon',
        'sass' => 'Inlude the SASS Files for building upon',
        'tukicons' => 'Adds support for the tuktuk icons',
        'path' => 'Save the theme in a custom directory',
      ),
    'examples' => array(
        'drush tuktuk NAME --less' => 'Generates the subtheme directory with LESS only files for compiling.',
        'drush tuktuk NAME --stylus' => 'Generates the subtheme directory with stylus only files for compiling.',
        'drush tuktuk NAME --sass' => 'Generates the subtheme directory with sass only files for compiling.',
        'drush tuktuk NAME --path="sites/all/themes"' => 'Specify a custom path to save the theme in from the drupal root (no trailing slash)',
        'drush tuktuk NAME --tukicons' => 'Will generate and download TukTuks custom font package into your subtheme files (NOT YET IMPLEMENTED)',
      ),
    'aliases' => array('tsub'),
    );

  $items['minify'] = array(
    'description' => 'Set up the tuktuk subtheme directory with a drush command.',
    'arguments' => array(
        'name' => 'The name of the theme to compile the CSS for.',
       ),
    'examples' => array(
        'drush minify Template' => 'This will minify all .CSS files in the CSS directory of the theme "Template".'
      ),
    'aliases' => array('tmin'),
    );
  return $items;
}

function tuktuk_drush_help($section) {
  switch ($section) {
    case 'drush:tuktuk':
      return dt('drush tuktuk helps set up the subtheme working directory');
      break;
    case 'drush:tsub':
      return dt('drush tuksub helps set up the subtheme working directory');
      break;
    case 'drush:minify':
      return dt('drush minify will minify all .CSS files in the CSS directory of a theme');
      break;
    case 'drush:tmin':
      return dt('drush tmin will minify all .CSS files in the CSS directory of a theme');
      break;
  }
}

function tidy_output($lines = array(), $splitterTop = FALSE, $splitterBot = FALSE) {
  // Will give some 'margin' above the message
  if ($splitterTop) {
    drush_print(dt(''));
  }

  // Compare the string lengths to get the largest string length
  $largestStringlength = max(array_map('strlen', $lines));

  // Create the comment seperaters based on the length of
  // the largest string
  $commentSep = '';
  $commentSepLen = $largestStringlength + 5;
  for ($i = 0; $i <= $commentSepLen; $i++) {
    $commentSep .= '=';
  }

  // Align the text items in the center
  $lineArray = array();
  $currentLine = 0;
  foreach($lines as $line => $l) {
    $stringLen = strlen($l);
    $divisorLen = $commentSepLen - $stringLen;
    $eitherSide = $divisorLen / 2;
    for ($i = 0; $i <= $eitherSide; $i++) {
      $l = ' ' . $l . ' ';
    }
    $lineArray[$currentLine] = $l;
    $currentLine++;
  }

  // Print the output
  drush_print(dt($commentSep));
  foreach($lineArray as $larray => $larry) {
    drush_print(dt($larry));
  }
  drush_print(dt($commentSep));

  // Will give some 'margin' below the message
  if ($splitterBot) {
    drush_print(dt(''));
  }
}


// Delete a directory and its files
function delete_files($target) {
  if(is_dir($target)){
      $files = glob( $target . '*', GLOB_MARK );
      foreach ( $files as $file ) {
        delete_files( $file );
      }
      rmdir( $target );
    } elseif (is_file($target)) {
      unlink( $target );
    }
}


/***************************************************************
 *
 *   drush tuktuk ...
 *   drush tuksub ...
 *
 ***************************************************************/

function drush_tuktuk($name = NULL) {
  // Check if the required arguments are NULL
  if (!isset($name)) {
    return tidy_output(array(
      'TukTuk requires that you enter a theme name',
      'Example:',
      'drush tuktuk <THEME_NAME>'
      ), FALSE, TRUE);
  }

  // Convert the
  $machine_friendly = strtolower($name);
  $machine_friendly = preg_replace('@[^a-z0-9_]+@', '_', $machine_friendly);

  // Check if the options have a value
  // --less
  if (drush_get_option('less')) {
    $less = drush_get_option('less');
  }
  // --stylus
  if (drush_get_option('stylus')) {
    $stylus = drush_get_option('stylus');
  }
  // --sass
  if (drush_get_option('sass')) {
    $sass = drush_get_option('sass');
  }
  // --tukicons
  if (drush_get_option('tukicons')) {
    $tukicons = drush_get_option('tukicons');
  }
  // --subpath
  $subpath = '/sites/all/themes';
  if (drush_get_option('path')) {
    $subpath = drush_get_option('path');
  }

  // Save the current directory
  $current_dir = getcwd();

  // Check if the libraries folder exists
  // If it doesn't, create it.
  $base_root = drush_normalize_path(drush_get_context('DRUSH_DRUPAL_ROOT'));
  if (!file_exists($base_root . '/sites/all/libraries')) {
    mkdir($base_root . '/sites/all/libraries', 0755, TRUE);
  }
  $library_dir = drush_normalize_path(drush_get_context('DRUSH_DRUPAL_ROOT')) . '/sites/all/libraries/';

  // Make a temporary directory
  $temp_gen = '_tuktuk_' . $machine_friendly . '_temp';
  if (!file_exists($library_dir . $temp_gen)) {
    mkdir($library_dir . $temp_gen, 0755, TRUE);
  }

  // Update $library_dir
  $library_dir_upd = $library_dir . $temp_gen;

  $base_dir = drush_normalize_path(drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $subpath);
  $subtheme_dir = drush_normalize_path(drush_get_context('DRUSH_DRUPAL_ROOT')) . '/' .drupal_get_path('theme', 'tuktuk') . '/tuktuk_subtheme';

  // Copy the subtheme directory to the libraries directory
  chdir($library_dir_upd);
  drush_copy_dir($subtheme_dir, $library_dir_upd, FILE_EXISTS_MERGE);

  // Pass on all variables to the theme builder function.
  $build_subtheme = drush_op('tuktuk_build_theme', $library_dir_upd, $machine_friendly, $stylus, $less, $sass, $tukicons);

  // Now that the theme is successfully generated, we can move it to the
  // directory specified by the user.
  error_reporting(0);
  mkdir($base_root . $subpath . '/' . $machine_friendly, 0755, TRUE);
  drush_copy_dir($library_dir . $temp_gen, $base_root . $subpath . '/' . $machine_friendly, FILE_EXISTS_MERGE);
  error_reporting(E_ALL);
  // Cleanup
  error_reporting(E_ALL^ E_WARNING);
  delete_files($library_dir);
  error_reporting(E_ALL);

  chdir($current_dir);
  return tidy_output(array(
    'The subtheme was successfully generated at:', $subpath . '/' . $machine_friendly),
    TRUE, TRUE);
}


function tuktuk_build_theme($lib_dir, $name = NULL, $stylus = NULL, $less = NULL, $sass = NULL, $tukicons = NULL) {
  // Check if the name is somehow NULL.
  if ($name == NULL) {
    return tidy_output(array(
      'A fatal error occurred while building the subtheme:',
      'Fatal Code 1'));
  }

  // .info file changes
  rename($lib_dir . '/tuktuk_subtheme.info.sub', $lib_dir . '/' . $name . '.info');
  // .info content generator

  $info = array();
  $info[1] = 'name = ' . $name;
  $info[2] = 'description = Subtheme generated through drush with TukTuk.';
  $info[3] = 'package = TukTuk';
  $info[4] = 'base theme = tuktuk';
  $info[5] = 'core = 7.x';
  // The array of regions to print
  $regions = array(
    'content',
    'highlighted',
    'sidebar_first',
    'sidebar_second',
    'footer',
    'page_top',
    'page_bottom',
    );
  $currentInfo = 6;
  foreach ($regions as $region => $r) {
    $info[$currentInfo] = 'regions[' . $r . '] = ' . $r;
    $currentInfo++;
  }
  // Stylesheets
  $info[$currentInfo] = 'stylesheets[all][] = css/style.css';

  // Put it all together
  foreach ($info as $inf => $in) {
    $gen_info .= $in . PHP_EOL;
  }

  // Place the string in the file.
  file_put_contents($lib_dir . '/' . $name . '.info', $gen_info);

  error_reporting(E_ALL^ E_WARNING);
  // If stylus is true then build the stylus files.
  if (!$stylus) {
    delete_files($lib_dir . '/stylus');
  }
  // If less is true then build the less files.
  if (!$less) {
    delete_files($lib_dir . '/LESS');
  }
  // If sass is true then build the sass files
  if (!$sass) {
    delete_files($lib_dir . '/sass');
  }
  // If tukicons is true then download them and apply in the info file.
  if ($tukicons) {
    // UNFINISHED
    // Download the tuktuk.icons.css and include them in the stylesheet
  }
  error_reporting(E_ALL);
}

/***************************************************************
 *
 *   drush minify ...
 *   drush tmin ...
 *
 ***************************************************************/

// @TODO - Implement Javascript Minification
function drush_tuktuk_minify($name = NULL) {
  if (!isset($name)) {
    return tidy_output(array(
      'TukTuk requires that you enter a theme name',
      'Example:',
      'drush minify <THEME_NAME>'
      ), TRUE, TRUE);
  }
  // Get the theme path.
  $path = drupal_get_path('theme', $name);

  // If the theme exists append /css/ onto the end.
  if ($path) {
    $path .= '/css/';
  } else {
    return tidy_output(array(
      'Theme name not found.',
      'Please check that the theme exists.'
      ), TRUE, TRUE);
  }
  // Get the relative path for the CSS files so the command can be run from anywhere.
  $path = drush_normalize_path(drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $path);

  // Scan the $path directory for all .css files.
  $dir = scandir($path);
  $cssFiles = array();
  // Append all .css files into an array to be used later.
  foreach ($dir as $d => $file) {
    if (strpos($file, '.css') != FALSE) {
      $cssFiles[$file] = $path . '/' . $file;
    } else {
      continue;
    }
  }

  // Copy all css files in to a backup dir so all can be used again.
  error_reporting(0);
  // Make the backup directory
  mkdir($path . '/backup', 0755, TRUE);
  // Copy all css files into the backup directory
  foreach ($cssFiles as $cssFile => $css) {
    copy($css, $path . '/backup/' . $cssFile);
  }
  error_reporting(E_ALL);

  // Minify the remaining stylesheets
  foreach($cssFiles as $cssFile => $css) {
    // Reset the buffer.
    $buffer = "";
    $buffer = file_get_contents($css);
    // Replace all spaces, new lines and comments with nothing.
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    $buffer = str_replace(': ', ':', $buffer);
    $buffer = str_replace(array(
      "\r\n",
      "\r",
      "\n",
      "\t",
      '  ',
      '    ',
      '    ',
      ), '', $buffer);
    // Put the contents back into the file
    file_put_contents($css, $buffer);
  }
  return tidy_output(array(
    "The .css files have been minified",
    "",
    "A backup can be found at:",
    $path . '/backup',
    ), TRUE, TRUE);
}
